<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config) die('Accès refusé');

?>
<h2>Paramètres de contrôle des accès</h2>
<form action="settings.php?t=access" method="post" id="save">
<?php csrf_token(); ?>
<input type="hidden" name="t" value="access" >
<table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4>Configurer l’accès à cette fenêtre d’assistance</h4>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th colspan="2">
                <em><b>Paramètres d’authentification de l’équipe</b></em>
            </th>
        </tr>
        <tr><td>Principes d’expiration des mots de passe</th>
            <td>
                <select name="passwd_reset_period">
                   <option value="0"> &mdash; Pas de date d’expiration &mdash;</option>
                  <?php
                    for ($i = 1; $i <= 12; $i++) {
                        echo sprintf('<option value="%d" %s>%s%s</option>',
                                $i,(($config['passwd_reset_period']==$i)?'selected="selected"':''), $i>1?"Every $i ":'', $i>1?' Months':'Monthly');
                    }
                    ?>
                </select>
                <font class="error"><?php echo $errors['passwd_reset_period']; ?></font>
                <i class="help-tip icon-question-sign" href="#password_expiration_policy"></i>
            </td>
        </tr>
        <tr><td>Autoriser la réinitialisation des mots de passe</th>
            <td>
              <input type="checkbox" name="allow_pw_reset" <?php echo $config['allow_pw_reset']?'checked="checked"':''; ?>>
              &nbsp;<i class="help-tip icon-question-sign" href="#allow_password_resets"></i>
            </td>
        </tr>
        <tr><td>Réinitialiser les jetons d’expiration</th> <!-- euh... je suis pas sûre, là -->
            <td>
              <input type="text" name="pw_reset_window" size="6" value="<?php
                    echo $config['pw_reset_window']; ?>">
                <em>mins</em>&nbsp;<i class="help-tip icon-question-sign" href="#reset_token_expiration"></i>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['pw_reset_window']; ?></font>
            </td>
        </tr>
        <tr><td>Maximum de connexions autorisées pour l’équipe</td>
            <td>
                <select name="staff_max_logins">
                  <?php
                    for ($i = 1; $i <= 10; $i++) {
                        echo sprintf('<option value="%d" %s>%d</option>', $i,(($config['staff_max_logins']==$i)?'selected="selected"':''), $i);
                    }
                    ?>
                </select> tentatives échouées de connexions autorisées avant que ne soit appliqué un blocage de 
                <select name="staff_login_timeout">
                  <?php
                    for ($i = 1; $i <= 10; $i++) {
                        echo sprintf('<option value="%d" %s>%d</option>', $i,(($config['staff_login_timeout']==$i)?'selected="selected"':''), $i);
                    }
                    ?>
                </select> minute(s).
            </td>
        </tr>
        <tr><td>Expiration de la session pour l’équipe</td>
            <td>
              <input type="text" name="staff_session_timeout" size=6 value="<?php echo $config['staff_session_timeout']; ?>">
                mins <em>( 0 to disable)</em>. <i class="help-tip icon-question-sign" href="#staff_session_timeout"></i>
            </td>
        </tr>
        <tr><td>Lier la session à l’adresse IP pour l’équipe</td>
            <td>
              <input type="checkbox" name="staff_ip_binding" <?php echo $config['staff_ip_binding']?'checked="checked"':''; ?>>
              <i class="help-tip icon-question-sign" href="#bind_staff_session_to_ip"></i>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                <em><b>Paramètres d’authentification pour l’utilisateur final</b></em>
            </th>
        </tr>
        <tr><td>Inscription obligatoire</td>
            <td><input type="checkbox" name="clients_only" <?php
                if ($config['clients_only'])
                    echo 'checked="checked"'; ?>/>
                Imposer l’inscription et la connexion pour créer des tickets
            <i class="help-tip icon-question-sign" href="#registration_method"></i>
            </td>
        <tr><td>Métode d’inscription</td>
            <td><select name="client_registration">
<?php foreach (array(
    'disabled' => 'Disabled — All users are guests',    /* si trad => Désactivé  — Tous les utilisateurs sont invités*/
    'public' => 'Public — Anyone can register',     /* si trad => Public  — Tout le monde peut s’inscrire*/
    'closed' => 'Private — Only staff can register users',)     /* si trad => Privé  — Seuls les membres de l’équipe peuvent inscrire les utilisateurs*/
    as $key=>$val) { ?>
        <option value="<?php echo $key; ?>" <?php
        if ($config['client_registration'] == $key)
            echo 'selected="selected"'; ?>><?php echo $val;
        ?></option><?php
    } ?>
            </select>
            <i class="help-tip icon-question-sign" href="#registration_method"></i>
            </td>
        </tr>
        <tr><td>Maximum de connexions autorisées pour les utilisateurs</td>
            <td>
                <select name="client_max_logins">
                  <?php
                    for ($i = 1; $i <= 10; $i++) {
                        echo sprintf('<option value="%d" %s>%d</option>', $i,(($config['client_max_logins']==$i)?'selected="selected"':''), $i);
                    }

                    ?>
                </select> tentatives échouées de connexions autorisées avant que ne soit appliqué un blocage de 
                <select name="client_login_timeout">
                  <?php
                    for ($i = 1; $i <= 10; $i++) {
                        echo sprintf('<option value="%d" %s>%d</option>', $i,(($config['client_login_timeout']==$i)?'selected="selected"':''), $i);
                    }
                    ?>
                </select> minute(s).
            </td>
        </tr>
        <tr><td>Expiration de la session pour l’utilisateur</td>
            <td>
              <input type="text" name="client_session_timeout" size=6 value="<?php echo $config['client_session_timeout']; ?>">
              <i class="help-tip icon-question-sign" href="#client_session_timeout"></i>
            </td>
        </tr>
        <tr><td>Accès client rapide</td>
            <td><input type="checkbox" name="client_verify_email" <?php
                if ($config['client_verify_email'])
                    echo 'checked="checked"'; ?>/>
                Demander un courriel de vérification sur la page «&nbsp;Vérifier le statut du ticket&nbsp;» page
            <i class="help-tip icon-question-sign" href="#client_verify_email"></i>
            </td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th colspan="2">
                <h4>Modèles de pages d’authentification et d’inscription</h4>
            </th>
        </tr>
    </thead>
    <tbody>
<?php
$res = db_query('select distinct(`type`), content_id, notes, name, updated from '
    .PAGE_TABLE
    .' where isactive=1 group by `type`');
$contents = array();
while (list($type, $id, $notes, $name, $u) = db_fetch_row($res))
    $contents[$type] = array($id, $name, $notes, $u);

$manage_content = function($title, $content) use ($contents) {
    list($id, $name, $notes, $upd) = $contents[$content];
    $notes = explode('. ', $notes);
    $notes = $notes[0];
    ?><tr><td colspan="2">
    <a href="#ajax.php/content/<?php echo $id; ?>/manage"
    onclick="javascript:
        $.dialog($(this).attr('href').substr(1), 200);
    return false;"><i class="icon-file-text pull-left icon-2x"
        style="color:#bbb;"></i> <?php
    echo Format::htmlchars($title); ?></a><br/>
        <span class="faded" style="display:inline-block;width:90%"><?php
        echo Format::display($notes); ?>
    <em>(Dernière mise à jour <?php echo Format::db_datetime($upd); ?>)</em></span></td></tr><?php
}; ?>
        <tr>
            <th colspan="2">
                <em><b>Modèles de pages d’authentication and et d’inscription</b></em>
            </th>
        </tr>
        <?php $manage_content('Staff Members', 'pwreset-staff'); ?>
        <?php $manage_content('Clients', 'pwreset-client'); ?>
        <?php $manage_content('Guess Ticket Access', 'access-link'); ?>
        <tr>
            <th colspan="2">
                <em><b>Pages de connexion</b></em>
            </th>
        </tr>
        <?php $manage_content('Staff Login Banner', 'banner-staff'); ?>
        <?php $manage_content('Client Sign-In Page', 'banner-client'); ?>
        <tr>
            <th colspan="2">
                <em><b>Inscription d’un compte utilisateur </b></em>
            </th>
        </tr>
        <?php $manage_content('Please Confirm Email Address Page', 'registration-confirm'); ?>
        <?php $manage_content('Confirmation Email', 'registration-client'); ?>
        <?php $manage_content('Account Confirmed Page', 'registration-thanks'); ?>
        <tr>
            <th colspan="2">
                <em><b>Inscription d’un compte de l’équipe</b></em>
            </th>
        </tr>
        <?php $manage_content('Staff Welcome Email', 'registration-staff'); ?>
</tbody>
</table>
<p style="text-align:center">
    <input class="button" type="submit" name="submit" value="Sauvegarder les modifications">
    <input class="button" type="reset" name="reset" value="Réinitialiser">
</p>
</form>
