
<h2>Installer un nouveau plug-in</h2>
<p>
Pour ajouter un plug-in au système, charger et placer le plug-in dans le répertoire 
<code>include/plugins</code>. Une fois que le plug-in sera dans le répertoire 
<code>plugins/</code>, il sera affiché dans la liste ci-dessous.
</p>

<form method="post" action="?">
    <?php echo csrf_token(); ?>
    <input type="hidden" name="do" value="install"/>
<table class="list" width="100%"><tbody>
<?php

$installed = $ost->plugins->allInstalled();
foreach ($ost->plugins->allInfos() as $info) {
    // Ignore installed plugins
    if (isset($installed[$info['install_path']]))
        continue;
    ?>
        <tr><td><button type="submit" name="install_path"
            value="<?php echo $info['install_path'];
            ?>">Installer</button></td>
        <td>
    <div><strong><?php echo $info['name']; ?></strong><br/>
        <div><?php echo $info['description']; ?></div>
        <div class="faded"><em>Version&nbsp;: <?php echo $info['version']; ?></em></div>
        <div class="faded"><em>Auteur&nbsp;: <?php echo $info['author']; ?></em></div>
    </div>
    </td></tr>
    <?php
}
?>
</tbody></table>
</form>
