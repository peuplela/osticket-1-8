<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config) die('Accès refusé');
?>
<h2>Paramètres et options de la base de connaissance</h2>
<form action="settings.php?t=kb" method="post" id="save">
<?php csrf_token(); ?>
<input type="hidden" name="t" value="kb" >
<table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4>Paramètres de la base de connaissance</h4>
                <em>Désactiver la base de connaissance désactivera l’interface clients.</em>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180">Statut de la bse de connaissance</td>
            <td>
              <input type="checkbox" name="enable_kb" value="1" <?php echo $config['enable_kb']?'checked="checked"':''; ?>>
              Activer la base de connaissance
              &nbsp;<font class="error">&nbsp;<?php echo $errors['enable_kb']; ?></font> <i class="help-tip icon-question-sign" href="#knowledge_base_status"></i>
            </td>
        </tr>
        <tr>
            <td width="180">Réponses prédéfinies</td>
            <td>
                <input type="checkbox" name="enable_premade" value="1" <?php echo $config['enable_premade']?'checked="checked"':''; ?> >
                Activer les réponses prédéfinies
                &nbsp;<font class="error">&nbsp;<?php echo $errors['enable_premade']; ?></font> <i class="help-tip icon-question-sign" href="#canned_responses"></i>
            </td>
        </tr>
    </tbody>
</table>
<p style="padding-left:210px;">
    <input class="button" type="submit" name="submit" value="Sauvegarder les modifications">
    <input class="button" type="reset" name="reset" value="Réinitialiser">
</p>
</form>
