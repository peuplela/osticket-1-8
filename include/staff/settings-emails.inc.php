<?php
if(!defined('OSTADMININC') || !$thisstaff || !$thisstaff->isAdmin() || !$config) die('Accès refusé');
?>
<h2>Paramètres et options des courriels</h2>
<form action="settings.php?t=emails" method="post" id="save">
<?php csrf_token(); ?>
<input type="hidden" name="t" value="emails" >
<table class="form_table settings_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th colspan="2">
                <h4>Paramètres des courriels</h4>
                <em>Veuillez noter que certains des paramètres généraux peuvent être annulés au niveau du département/du courriel.</em> <!-- toujours pas plus clair pour moi ce 'department/email level' -->
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="180" class="required">Jeu de gabarits par défaut</td>
            <td>
                <select name="default_template_id">
                    <option value="">&mdash; Sélectionner un jeu de gabarits pour les courriel &mdash;</option>
                    <?php
                    $sql='SELECT tpl_id, name FROM '.EMAIL_TEMPLATE_GRP_TABLE
                        .' WHERE isactive =1 ORDER BY name';
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        while (list($id, $name) = db_fetch_row($res)){
                            $selected = ($config['default_template_id']==$id)?'selected="selected"':''; ?>
                            <option value="<?php echo $id; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
                        <?php
                        }
                    } ?>
                </select>&nbsp;<font class="error">*&nbsp;<?php echo $errors['default_template_id']; ?></font>
                <i class="help-tip icon-question-sign" href="#default_email_templates"></i>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">Courriel par défaut du système</td>
            <td>
                <select name="default_email_id">
                    <option value=0 disabled>Sélectionner une adressee courriel/option>
                    <?php
                    $sql='SELECT email_id,email,name FROM '.EMAIL_TABLE;
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        while (list($id,$email,$name) = db_fetch_row($res)){
                            $email=$name?"$name &lt;$email&gt;":$email;
                            ?>
                            <option value="<?php echo $id; ?>"<?php echo ($config['default_email_id']==$id)?'selected="selected"':''; ?>><?php echo $email; ?></option>
                        <?php
                        }
                    } ?>
                 </select>
                 &nbsp;<font class="error">*&nbsp;<?php echo $errors['default_email_id']; ?></font>
                <i class="help-tip icon-question-sign" href="#default_system_email"></i>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">Alerte courriel par défaut</td>
            <td>
                <select name="alert_email_id">
                    <option value="0" selected="selected">Utiliser le courriel par défaut du système (ci-dessus)</option>
                    <?php
                    $sql='SELECT email_id,email,name FROM '.EMAIL_TABLE.' WHERE email_id != '.db_input($config['default_email_id']);
                    if(($res=db_query($sql)) && db_num_rows($res)){
                        while (list($id,$email,$name) = db_fetch_row($res)){
                            $email=$name?"$name &lt;$email&gt;":$email;
                            ?>
                            <option value="<?php echo $id; ?>"<?php echo ($config['alert_email_id']==$id)?'selected="selected"':''; ?>><?php echo $email; ?></option>
                        <?php
                        }
                    } ?>
                 </select>
                 &nbsp;<font class="error">*&nbsp;<?php echo $errors['alert_email_id']; ?></font>
                <i class="help-tip icon-question-sign" href="#default_alert_email"></i>
            </td>
        </tr>
        <tr>
            <td width="180" class="required">Adresse courriel de l’admin</td>
            <td>
                <input type="text" size=40 name="admin_email" value="<?php echo $config['admin_email']; ?>">
                    &nbsp;<font class="error">*&nbsp;<?php echo $errors['admin_email']; ?></font>
                <i class="help-tip icon-question-sign" href="#admins_email_address"></i>
            </td>
        </tr>
        <tr><th colspan=2><em><strong>Courriels entrants</strong>&nbsp;
            </em></th>
        <tr>
            <td width="180">Recherche des adresses courriel</td>
            <td><input type="checkbox" name="enable_mail_polling" value=1 <?php echo $config['enable_mail_polling']? 'checked="checked"': ''; ?>  > Activer
                <i class="help-tip icon-question-sign" href="#email_fetching"></i>
                &nbsp;
                 <input type="checkbox" name="enable_auto_cron" <?php echo $config['enable_auto_cron']?'checked="checked"':''; ?>>
                 Recherche automatique par croncron&nbsp;
                <i class="help-tip icon-question-sign" href="#enable_autocron_fetch"></i>
            </td>
        </tr>
        <tr>
            <td width="180">Effacer la réponse citée</td> <!-- si j'ai bien compris ici http://osticket.com/forum/discussion/420/strip-quoted-reply-and-reply-separator-tag -->
            <td>
                <input type="checkbox" name="strip_quoted_reply" <?php echo $config['strip_quoted_reply'] ? 'checked="checked"':''; ?>>
                Activer <i class="help-tip icon-question-sign" href="#strip_quoted_reply"></i>
                &nbsp;<font class="error">&nbsp;<?php echo $errors['strip_quoted_reply']; ?></font>
            </td>
        </tr>
        <tr>
            <td width="180">Marque de séparation de la réponse</td>
            <td><input type="text" name="reply_separator" value="<?php echo $config['reply_separator']; ?>">
                &nbsp;<font class="error">&nbsp;<?php echo $errors['reply_separator']; ?></font>&nbsp;<i class="help-tip icon-question-sign" href="#reply_separator_tag"></i>
            </td>
        </tr>
        <tr>
            <td width="180">Priorités de courriel pour les tickets</td>
            <td>
                <input type="checkbox" name="use_email_priority" value="1" <?php echo $config['use_email_priority'] ?'checked="checked"':''; ?> >&nbsp;Acitver&nbsp;
                <i class="help-tip icon-question-sign" href="#emailed_tickets_priority"></i>
            </td>
        </tr>
        <tr>
            <td width="180">Accepter tous les courriels</td>
            <td><input type="checkbox" name="accept_unregistered_email" <?php
                echo $config['accept_unregistered_email'] ? 'checked="checked"' : ''; ?>/>
                Accepter les courriels d’utilisateurs inconnus
                <i class="help-tip icon-question-sign" href="#accept_all_emails"></i>
            </td>
        </tr>
        <tr>
            <td width="180">Accepter les courriels des collaborateurs</td>
            <td><input type="checkbox" name="add_email_collabs" <?php
    echo $config['add_email_collabs'] ? 'checked="checked"' : ''; ?>/>
            Ajouter automatiquement les collaborateurs à partir des champs de courriel&nbsp;
            <i class="help-tip icon-question-sign" href="#accept_email_collaborators"></i>
        </tr>
        <tr><th colspan=2><em><strong>Courriels sortants</strong>&nbsp;: par défaut ne s’applique qu’aux courriels sortants sans paramètres SMTP.</em></th></tr>
        <tr><td width="180">MTA (<em>Mail Transfer Agent</em>) par défaut</td>
            <td>
                <select name="default_smtp_id">
                    <option value=0 selected="selected">Aucun&nbsp;: utiliser la fonction mail() en PHP</option> <!-- baliser  'mail()' comme étant du code ? -->
                    <?php
                    $sql=' SELECT email_id, email, name, smtp_host '
                        .' FROM '.EMAIL_TABLE.' WHERE smtp_active = 1';
                    if(($res=db_query($sql)) && db_num_rows($res)) {
                        while (list($id, $email, $name, $host) = db_fetch_row($res)){
                            $email=$name?"$name &lt;$email&gt;":$email;
                            ?>
                            <option value="<?php echo $id; ?>"<?php echo ($config['default_smtp_id']==$id)?'selected="selected"':''; ?>><?php echo $email; ?></option>
                        <?php
                        }
                    } ?>
                 </select>&nbsp;<font class="error">&nbsp;<?php echo $errors['default_smtp_id']; ?></font>
                 <i class="help-tip icon-question-sign" href="#default_mta"></i>
           </td>
       </tr>
    </tbody>
</table>
<p style="padding-left:250px;">
    <input class="button" type="submit" name="submit" value="Sauvegarder les modifications">
    <input class="button" type="reset" name="reset" value="Réinitialiser">
</p>
</form>
