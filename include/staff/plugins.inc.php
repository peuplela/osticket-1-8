<div style="width:700;padding-top:5px; float:left;">
 <h2>Plug-in insatllés actuellement</h2>
</div>
<div style="float:right;text-align:right;padding-top:5px;padding-right:5px;">
 <b><a href="plugins.php?a=add" class="Icon form-add">Ajouter un plug-in</a></b></div>
<div class="clear"></div>

<?php
$page = ($_GET['p'] && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
$count = count($ost->plugins->allInstalled());
$pageNav = new Pagenate($count, $page, PAGE_LIMIT);
$pageNav->setURL('forms.php');
$showing=$pageNav->showing().' forms';
?>

<form action="plugins.php" method="POST" name="forms">
<?php csrf_token(); ?>
<input type="hidden" name="do" value="mass_process" >
<input type="hidden" id="action" name="a" value="" >
<table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
    <thead>
        <tr>
            <th width="7">&nbsp;</th>
            <th>Nom du plug-in</th>
            <th>Statut</td>
            <th>Date d’installation</th>
        </tr>
    </thead>
    <tbody>
<?php
foreach ($ost->plugins->allInstalled() as $p) {
    if ($p instanceof Plugin) { ?>
    <tr>
        <td><input type="checkbox" class="ckb" name="ids[]" value="<?php echo $p->getId(); ?>"
                <?php echo $sel?'checked="checked"':''; ?>></td>
        <td><a href="plugins.php?id=<?php echo $p->getId(); ?>"
            ><?php echo $p->getName(); ?></a></td>
        <td><?php echo ($p->isActive())
            ? 'Enabled' : '<strong>Désactivé</strong>'; ?></td>
        <td><?php echo Format::db_datetime($p->getInstallDate()); ?></td>
    </tr>
    <?php } else {} ?>
<?php } ?>
    </tbody>
    <tfoot>
     <tr>
        <td colspan="4">
            <?php if($count){ ?>
            Sélectionner&nbsp;
            <a id="selectAll" href="#ckb">Tout</a>&nbsp;&nbsp;
            <a id="selectNone" href="#ckb">Aucun</a>&nbsp;&nbsp;
            <a id="selectToggle" href="#ckb">Basculer</a>&nbsp;&nbsp;
            <?php }else{
                echo 'Aucun plug-in n’a été installé pour l’instant &mdash; <a href="?a=add">installez-en un&nbsp;</a>!';
            } ?>
        </td>
     </tr>
    </tfoot>
</table>
<?php
if ($count) //Show options..
    echo '<div>&nbsp;Page:'.$pageNav->getPageLinks().'&nbsp;</div>';
?>
<p class="centered" id="actions">
    <input class="button" type="submit" name="delete" value="Supprimer">
    <input class="button" type="submit" name="enable" value="Activer">
    <input class="button" type="submit" name="disable" value="Désactiver">
</p>
</form>

<div style="display:none;" class="dialog" id="confirm-action">
    <h3>Veuillez confirmer</h3>
    <a class="close" href="">&times;</a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong>Êtes-vous sûr de vouloir SUPPRIMER les plug-in sélectionnés&nbsp;?</strong></font>
        <br><br>Les formulaires supprimés ne POURRONT PAS être récupérés. <!-- Une erreur d’origine ? ('deleted forms'), est-ce qu’il ne faudrait pas remplacer par plug-in ? -->
    </p>
    <p class="confirm-action" style="display:none;" id="enable-confirm">
        <font color="green"><strong>Êtes-vous prêt.e à activer les plug-in sélectionnés&nbsp;?</strong></font>
    </p>
    <p class="confirm-action" style="display:none;" id="disable-confirm">
        <font color="red"><strong>Êtes-vous sûr.e de vouloir désactiver les plug-in sélectionnés&nbsp;?</strong></font>
    </p>
    <div>Veuillez confirmer pour continuer</div>
    <hr style="margin-top:1em"/>
    <p class="full-width">
        <span class="buttons" style="float:left">
            <input type="button" value="Non, annuler" class="close">
        </span>
        <span class="buttons" style="float:right">
            <input type="button" value="Oui, je confirme&nbsp;!" class="confirm">
        </span>
     </p>
    <div class="clear"></div>
</div>
