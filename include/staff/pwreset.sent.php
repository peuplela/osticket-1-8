<?php
include_once(INCLUDE_DIR.'staff/login.header.php');
defined('OSTSCPINC') or die('chemin non valide');
$info = ($_POST && $errors)?Format::htmlchars($_POST):array();
?>

<div id="loginBox">
    <h1 id="logo"><a href="index.php">osTicket - Réinitialisation des mots de passe par l’équipe/a></h1> <!-- 'Staff password' => trad alternative, à confirmer -->
    <h3>Un courriel de confirmation a été envoyé</h3>
    <h3 style="color:black;"><em>
    Un courriel de réinitialisation de mot de passe a été envoyé à l’adresse courriel enregistrée e-mail pour votre compte.
    Veuillez suivre le lien contenu dans ce courriel pour réinitialiser votre mot de passe.
    </em></h3>

    <form action="index.php" method="get">
        <input class="submit" type="submit" name="submit" value="Connexion"/>
    </form>
</div>

<div id="copyRights">Copyright &copy; <a href='http://www.osticket.com' target="_blank">osTicket.com</a></div> <!-- le retour de la revanche, etc.-->
</body>
</html>
