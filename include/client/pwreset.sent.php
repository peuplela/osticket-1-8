<h1>J'ai oublié mon mot de passe</h1>
<p>
Saisissez votre nom d'utilisateur ou votre adresse électronique dans les champs ci-dessous et cliquez sur
le bouton <strong>Envoyer par mail</strong> pour recevoir à votre adresse un lien de réinitialisation de votre mot de passe.

<form action="pwreset.php" method="post" id="clientLogin">
    <div style="width:50%;display:inline-block">
    Nous vous avons envoyé un mail avec un lien de réinitialisation pour votre compte. Si vous n'avez pas reçu ce mail ou ne parvenez pas à réinitialiser votre mot de passe, veuillez…
    </div>
</form>
