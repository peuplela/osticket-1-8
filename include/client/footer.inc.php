        </div>
    </div>
    <div id="footer">
        <p>Copyright &copy; <?php echo date('Y'); ?> <?php echo (string) $ost->company ?: 'osTicket.com'; ?> - Tous droits réservés.</p>
        <a id="poweredBy" href="http://osticket.com" target="_blank">Helpdesk software - powered by osTicket</a>
    </div>
<div id="overlay"></div>
<div id="loading">
    <h4>Veuillez patienter…</h4>
    <p>Merci de patienter… cela prendra quelques instants&nbsp;!</p>
</div>
</body>
</html>
